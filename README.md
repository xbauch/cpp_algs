## Books to be covered

- The Art of Computer Programming (Knuth)
- Algorithms (Erickson)
- Introduction to Algorithms (Cormen, Leiserson, Rivest, Stein)
- Programming Perls (Bentley)
- Hacker's Delight (Warren)
