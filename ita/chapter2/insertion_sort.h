#include <vector>

using std::vector;

template<typename T>
void sort( vector< T > &v ) {
  for ( int j = 2; j < v.size(); ++j ) {
    T key = v[ j ];
    int i = j - 1;
    while ( i > 0 && v[ i ] > key ) {
      v[ i + 1 ] = v[ i ];
      --i;
    }
    v[ i + 1 ] = key;
  }
}
